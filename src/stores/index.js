import { configureStore } from '@reduxjs/toolkit';
import reducer from './chats/reducer';

export const store = configureStore({
    reducer: {
        chatReducer: reducer,
    },
});
