import {CREATE, GET, NEW_USER, SHOW} from './constant';

export const initialState = {
    chats: [],
    messages: [],
    contacts: [],
};

function reducer(state = {}, action) {
    let newState
    switch (action.type) {
        case GET:
            newState = {
                ...state,
                chats: initialState.chats
            }
            break;
        case SHOW:
            newState = {
                ...state,
                messages: initialState.chats[action.payload]
            }
            break;
        case CREATE:
            newState = {
                ...state,
                chats: action.chats
            }
            break;
        case NEW_USER:
            newState = {
                ...state,
                contacts: action.contacts
            }
            break;
        default:
            newState = initialState
            break;
    }
    return newState
}

export default reducer
