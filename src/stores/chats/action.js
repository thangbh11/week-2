import {CREATE, GET, SHOW} from "./constant";

export const get = () => {
    return {
        type: GET,
    }
}

export const create = payload => {
    return {
        type: CREATE,
        payload
    }
}

export const show = payload => {
    return {
        type: SHOW,
        payload
    }
}
