import {Link, useParams} from "react-router-dom";
import '../UserChat/UserChat.scss'
import {useCallback, useEffect, useRef, useState} from "react";
import {CREATE, NEW_USER} from "../../stores/chats/constant";
import {useDispatch, useSelector} from "react-redux";
import HistoryChat from "../HistoryChat";
import socketIOClient from "socket.io-client";
import Contact from "../Contact";

const AdminChat = (props) => {
    const { id } = useParams();
    const [message, setMessage] = useState('')
    const chats = useSelector(state => state.chatReducer.chats)
    const dispatch = useDispatch()
    const socketRef = useRef();
    const contacts = useSelector(state => state.chatReducer.contacts)
    useEffect(() => {
        socketRef.current = socketIOClient.connect("http://localhost:3000")

        socketRef.current.on('sendDataServer', dataGot => {
            const newChats = chats.concat(dataGot.data)
            dispatch({ type: CREATE, chats: newChats})
        })

        socketRef.current.on('newContact', dataGot => {
            const newContact = dataGot.data
            let newData = true;
            let newContacts = contacts.map(item => {
                if (item.chat_id === newContact.chat_id) {
                    newData = false;
                    return newContact;
                }
            })

            if (newData) {
                newContacts = newContacts.concat(newContact)
            }
            dispatch({ type: NEW_USER, contacts: newContacts})
        })

        return () => {
            socketRef.current.disconnect();
        };
    }, [chats, contacts]);

    const handeClick = () => {
        const chat = {
            chat_id: id,
            from: 'admin',
            to: id,
            content: message
        }
        const newChats = chats.concat(chat)
        dispatch({ type: CREATE, chats: newChats})
        socketRef.current.emit('sendDataClient', chat)
        setMessage('')
    }

    const messages = chats.filter(item => item.chat_id === id)

    return (
        <div className={'row'}>
            <div className="col-4">
                <Contact contacts={contacts} id={id} socket={socketRef}/>
            </div>
            <div className="col-8">
                {
                    id && (
                        <div className="rounded border border-dark d-flex flex-column">
                            <HistoryChat chats={messages} id={'admin'} />
                            <div className="p-2 d-flex">
                                <input type="text" value={message} onInput={e => setMessage(e.target.value)} className={'input-chat rounded border border-dark me-2 px-2'} />
                                <button className={'btn btn-primary'} onClick={handeClick}>
                                    Send
                                </button>
                            </div>
                        </div>
                    )
                }
            </div>
        </div>
    )
}

export default AdminChat
