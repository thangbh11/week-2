import {memo, useCallback, useState} from "react";
import {Link} from "react-router-dom";
import UserModal from "./UserModal";

const Contact = (props) => {
    const [modal, setModal] = useState({
        show: false,
        id: null
    })

    const handleClose = useCallback(() => {
        setModal({
            show: false,
            id: null
        })
    }, [])

    const handleSave = useCallback((contact) => {
        props.socket.current.emit('postContact', contact)
        setModal({
            show: false,
            id: null
        })
    }, [])

    return (
        <div>
            {props.contacts && props.contacts.map((item, key) => (
                <Link to={'/admins/' + item.chat_id} key={key}>
                    <button className={'w-100 btn mt-2 ' + (item.chat_id === props.id ? 'btn-primary' : 'btn-light')}>
                        <span>{item.name ?? item.chat_id}</span>
                        <span className={'btn-edit'} onClick={() => setModal({
                            show: true,
                            id: item.chat_id
                        })}>edit</span>
                    </button>
                </Link>
            ))}
            <UserModal show={modal.show} id={modal.id} onModalClose={handleClose} onSave={handleSave}/>
        </div>
    )
}

export default memo(Contact)
