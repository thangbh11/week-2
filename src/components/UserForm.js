import React, {useRef, useState} from "react";
import {useNavigate} from "react-router-dom";
import socketIOClient from "socket.io-client";

const UserForm = (props) => {
    const [isDisabled, setIsDisabled] = useState(true)
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const navigate = useNavigate();
    const socketRef = useRef();

    socketRef.current = socketIOClient.connect("http://localhost:3000")

    const checkActive = (name, phone) => {
        if (name && phone.length > 9) {
            setIsDisabled(false)
        }
    }
    const handleClick = () => {
        const contact = {
            chat_id: props,
            name: name,
            address: '',
            gender: '',
            note: ''
        }
        socketRef.current.emit('postContact', contact)
        navigate('/users/' + phone)
    }

    return (
        <div className="row pt-5">
            <div className="col-4 offset-4">
                <div className="rounded border border-dark p-3">
                    <div className="form-outline mb-4">
                        <label className="form-label" htmlFor="name">Name</label>
                        <input type="name" id="name" value={name} onChange={(e) => {
                            setName(e.target.value);
                            checkActive(e.target.value, phone);
                        }} className="form-control"/>
                    </div>
                    <div className="form-outline mb-4">
                        <label className="form-label" htmlFor="phone">Phone</label>
                        <input type="phone" id="phone" value={phone} onChange={(e) => {
                            setPhone(e.target.value);
                            checkActive(name, e.target.value);
                        }} className="form-control"/>
                    </div>
                    <button type="submit" disabled={isDisabled} onClick={handleClick} className="btn btn-primary btn-block mb-4">Connect</button>
                </div>
            </div>
        </div>
    )
}


export default UserForm
