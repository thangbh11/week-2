import {memo} from "react";

const HistoryChat = (props) => {
    return (
        <div className="history-chat p-2">
            {
                props.chats && props.chats.map((item, key) => (
                    <div key={key} className={'d-flex mt-1 chat ' + (item.from === props.id ? 'justify-content-end' : '')}>
                        <span className={'px-2 rounded text-white ' + (item.from === props.id ? 'bg-primary' : 'bg-secondary')}>{item.content}</span>
                    </div>
                ))
            }
        </div>
    )
}

export default memo(HistoryChat)
