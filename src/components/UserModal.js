import {memo, useEffect, useState} from "react";
import {useSelector} from "react-redux";

const UserModal = (props) => {
    const contacts = useSelector(state => state.chatReducer.contacts)
    const internalContact = contacts.find(ele => ele.chat_id === props.id);
    const [contact, setContact] = useState({
        chat_id: props.id,
        name: '',
        address: '',
        gender: '',
        note: ''
    })

    useEffect(() => {
        setContact(internalContact);
    }, [internalContact])

    return (
        <div className={'modal ' + (props.show ? 'show' : '')} tabIndex="-1">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">User information</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        {
                            contact && (
                                <>
                                    <div className="form-group mt-2">
                                        <label>Name</label>
                                        <input type="text" className={'form-control'} value={contact.name} onChange={e => setContact({
                                            ...contact,
                                            name: e.target.value
                                        })}/>
                                    </div>

                                    <div className="form-group mt-2">
                                        <label>Phone</label>
                                        <input type="text" className={'form-control'} value={contact.chat_id} readOnly={true}/>
                                    </div>

                                    <div className="form-group mt-2">
                                        <label>Address</label>
                                        <input type="text" className={'form-control'} value={contact.address} onChange={e => setContact({
                                            ...contact,
                                            address: e.target.value
                                        })}/>
                                    </div>

                                    <div className="form-group mt-2">
                                        <label>Gender</label>
                                        <select className={'form-control'} value={contact.gender} onChange={e => setContact({
                                            ...contact,
                                            gender: e.target.value
                                        })}>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>

                                    <div className="form-group mt-2">
                                        <label>Note</label>
                                        <textarea className={'form-control'} onChange={e => setContact({
                                            ...contact,
                                            note: e.target.value
                                        })} defaultValue={contact.note}></textarea>
                                    </div>
                                </>
                            )
                        }
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={() => props.onModalClose()}>Close</button>
                        <button type="button" className="btn btn-primary" onClick={() => props.onSave(contact)}>Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default memo(UserModal)
