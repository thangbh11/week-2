import React, {useState} from "react";
import {useNavigate} from "react-router-dom";

const UserForm = (props) => {
    const [isDisabled, setIsDisabled] = useState(true)
    const [name, setName] = useState('')
    const [password, setPassword] = useState('')
    const navigate = useNavigate();

    const checkActive = (name, phone) => {
        if (name && password) {
            setIsDisabled(false)
        }
    }
    const handleClick = () => {
        navigate('/admins')
    }

    return (
        <div className="row pt-5">
            <div className="col-4 offset-4">
                <div className="rounded border border-dark p-3">
                    <div className="form-outline mb-4">
                        <label className="form-label" htmlFor="name">Name</label>
                        <input type="name" id="name" value={name} onChange={(e) => {
                            setName(e.target.value);
                            checkActive(e.target.value, password);
                        }} className="form-control"/>
                    </div>
                    <div className="form-outline mb-4">
                        <label className="form-label" htmlFor="phone">Password</label>
                        <input type="password" id="phone" value={password} onChange={(e) => {
                            setPassword(e.target.value);
                            checkActive(name, e.target.value);
                        }} className="form-control"/>
                    </div>
                    <button type="submit" disabled={isDisabled} onClick={handleClick} className="btn btn-primary btn-block mb-4">Connect</button>
                </div>
            </div>
        </div>
    )
}


export default UserForm
