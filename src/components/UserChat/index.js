import {useParams} from "react-router-dom";
import './UserChat.scss'
import {useEffect, useReducer, useRef, useState} from "react";
import {CREATE} from "../../stores/chats/constant";
import {useDispatch, useSelector} from "react-redux";
import HistoryChat from "../HistoryChat";
import socketIOClient from "socket.io-client";

const UserChat = (props) => {
  const { id } = useParams();
  const [message, setMessage] = useState('')
  const chats = useSelector(state => state.chatReducer.chats)
  const dispatch = useDispatch()
  const socketRef = useRef();

  useEffect(() => {
    socketRef.current = socketIOClient.connect("http://localhost:3000")

    socketRef.current.on('sendDataServer', dataGot => {
      const newChats = chats.concat(dataGot.data)
      dispatch({ type: CREATE, chats: newChats})
    })

    return () => {
      socketRef.current.disconnect();
    };
  }, [chats]);

  const handeClick = () => {
    const chat = {
      chat_id: id,
      from: id,
      to: 'admin',
      content: message
    }
    const newChats = chats.concat(chat)
    dispatch({ type: CREATE, chats: newChats})
    socketRef.current.emit('sendDataClient', chat)
    setMessage('')
  }

  const messages = chats.filter(item => item.chat_id === id)

  return (
    <div className={'row'}>
      <div className="col-8 offset-2">
        <div className="rounded border border-dark d-flex flex-column">
          <HistoryChat chats={messages} id={id} />
          <div className="p-2 d-flex">
            <input type="text" value={message} onInput={e => setMessage(e.target.value)} className={'input-chat rounded border border-dark me-2 px-2'} />
            <button className={'btn btn-primary'} onClick={handeClick}>
              Send
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default UserChat
