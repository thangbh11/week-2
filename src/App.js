import React from 'react';
import './App.css';
import UserForm from "./components/UserForm";
import {Route, Routes} from "react-router-dom";
import UserChat from "./components/UserChat";
import AdminChat from "./components/AdminChat";
import AdminForm from "./components/AdminForm";

function App() {
  return (
    <div className="container mt-5">
      <Routes>
        <Route path="/" element={<UserForm />} />
        <Route path="users/:id" element={<UserChat />} />
        <Route path="admins/:id" element={<AdminChat />} />
        <Route path="admins" element={<AdminChat />} />
        <Route path="login" element={<AdminForm />} />
      </Routes>
    </div>
  );
}

export default App;
